// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";

// import Bootstrap JavaScript files
import "bootstrap/js/dist/modal";
// Add more Bootstrap components if needed

// start the Stimulus application
import "./bootstrap";
