$(document).ready(function () {
  // Écouteur d'événement pour le clic sur un utilisateur Un écouteur d'événement de clic est attaché à tous les éléments avec la classe CSS .col-sm-3. Cela signifie que chaque fois qu'un utilisateur clique sur une carte d'utilisateur, le code à l'intérieur de cette fonction sera exécuté.
  $(".col-sm-3").on("click", function () {
    //Cette ligne récupère l'attribut data-user-id de l'élément sur lequel l'utilisateur a cliqué. Cet attribut contient l'identifiant unique de l'utilisateur.

    var userId = $(this).data("user-id");

    // envoie une requête AJAX vers l'URL spécifiée pour récupérer les détails de l'utilisateur correspondant à l'ID extrait.
    $.ajax({
      url: "/get_user_details", // L'URL de votre endpoint pour récupérer les détails de l'utilisateur
      method: "GET",
      data: { userId: userId },

      //Cette fonction est exécutée si la requête AJAX réussit. La réponse du serveur est passée en tant que paramètre
      success: function (response) {
        // Traitez la réponse de la requête AJAX ici
        console.log(response);
      },

      //Cette fonction est exécutée si la requête AJAX échoue. Les erreurs sont affichées dans la console avec
      error: function (xhr, status, error) {
        // Traitez les erreurs de la requête AJAX ici
        console.error(error);
      },
    });
  });
});
