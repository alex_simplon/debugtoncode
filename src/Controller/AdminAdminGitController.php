<?php

namespace App\Controller;

use App\Entity\Git;
use App\Form\Git1Type;
use App\Repository\GitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/admin/git')]
class AdminAdminGitController extends AbstractController
{
    #[Route('/', name: 'app_admi_admin_git_index', methods: ['GET'])]
    public function index(GitRepository $gitRepository): Response
    {
        return $this->render('admi_admin_git/index.html.twig', [
            'gits' => $gitRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admi_admin_git_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GitRepository $gitRepository): Response
    {
        $git = new Git();
        $form = $this->createForm(Git1Type::class, $git);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gitRepository->save($git, true);

            return $this->redirectToRoute('app_admi_admin_git_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admi_admin_git/new.html.twig', [
            'git' => $git,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admi_admin_git_show', methods: ['GET'])]
    public function show(Git $git): Response
    {
        return $this->render('admi_admin_git/show.html.twig', [
            'git' => $git,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admi_admin_git_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Git $git, GitRepository $gitRepository): Response
    {
        $form = $this->createForm(Git1Type::class, $git);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gitRepository->save($git, true);

            return $this->redirectToRoute('app_admi_admin_git_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admi_admin_git/edit.html.twig', [
            'git' => $git,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admi_admin_git_delete', methods: ['POST'])]
    public function delete(Request $request, Git $git, GitRepository $gitRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $git->getId(), $request->request->get('_token'))) {
            $gitRepository->remove($git, true);
        }

        return $this->redirectToRoute('app_admi_admin_git_index', [], Response::HTTP_SEE_OTHER);
    }
}
