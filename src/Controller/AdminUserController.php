<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;


#[Route('/admin/user')]
class AdminUserController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    #[Route('/', name: 'app_admin_user_index', methods: ['GET'])]
    public function index(): Response
    {
        // Logique de traitement pour la méthode "index"

        return $this->render('admin_user/index.html.twig');
    }


    #[Route('/menu', name: 'app_admin_user_menu', methods: ['GET'])]
    public function menu(UserRepository $userRepository, Security $security): Response
    {
        $currentUser = $security->getUser();
        $users = [$currentUser]; // Remplacer cette ligne par le code pour récupérer la liste des utilisateurs appropriée

        return $this->render('admin_user/menu.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route('/profil', name: 'app_admin_user_profil', methods: ['GET'])]
    public function profil(): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            throw new AccessDeniedException('Vous devez vous connecter pour accéder à cette page.');
        }

        return $this->render('admin_user/profil.html.twig');
    }



    #[Route('/new', name: 'app_admin_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_admin_user_menu', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_user_show', methods: ['GET'])]
    public function show(User $user, TokenStorageInterface $tokenStorage, Security $security): Response
    {
        if (!$security->isGranted('ROLE_USER')) {
            return $this->render('admin_user/error.html.twig');
        }

        $currentUser = $tokenStorage->getToken()->getUser();
        if ($currentUser !== $user) {
            throw new AccessDeniedException('Accès refusé.');
        }

        return $this->render('admin_user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserRepository $userRepository, TokenStorageInterface $tokenStorage): Response
    {
        $currentUser = $tokenStorage->getToken()->getUser();
        if ($currentUser !== $user) {
            throw new AccessDeniedException('Accès refusé.');
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_admin_user_menu', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }


    public function editPassword(Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            $user->setPassword($plainPassword, $passwordHasher);

            $entityManager->flush();

            return $this->redirectToRoute('app_admin_user_menu');
        }

        return $this->render('Admin_User/edit_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }





    #[Route('/delete/{id}', name: 'app_admin_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, TokenStorageInterface $tokenStorage): Response
    {
        $token = $tokenStorage->getToken();

        if ($token === null) {
            throw new AccessDeniedException('Accès refusé.');
        }

        $currentUser = $token->getUser();

        if ($currentUser !== $user) {
            throw new AccessDeniedException('Accès refusé.');
        }

        $tokenStorage->setToken(null);
        $request->getSession()->invalidate();

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_accueil');
    }
}
