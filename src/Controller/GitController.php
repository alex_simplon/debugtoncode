<?php

namespace App\Controller;

use App\Entity\Git;
use App\Form\GitType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class GitController extends AbstractController
{
    private $entityManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    /**
     * @Route("/git", name="app_git")
     * @Route("/git", name="git_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        // Crée une nouvelle instance de l'entité Git
        $git = new Git();

        // Crée un formulaire basé sur le formulaire GitType et relie l'entité Git
        $form = $this->createForm(GitType::class, $git);

        // Gère la soumission du formulaire et les données de la requête
        $form->handleRequest($request);

        // Vérifie si le formulaire a été soumis et si les données sont valides
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère l'utilisateur actuellement connecté
            $user = $this->getUser();

            // Associe l'utilisateur et son email à l'entité Git
            $git->setUser($user);
            $email = $user->getEmail();
            $git->setEmail($email);

            // Enregistre l'entité Git dans la base de données
            $this->entityManager->persist($git);
            $this->entityManager->flush();

            // Envoie un e-mail à l'utilisateur
            $this->sendEmailToUser($email, $git);

            // Redirige vers la page de succès
            return $this->redirectToRoute('success_git');
        }

        // Affiche le formulaire et la vue associée
        return $this->render('git/index.html.twig', [
            'controller_name' => 'GitController',
            'form' => $form->createView(),
        ]);
    }

    // Fonction privée pour envoyer un e-mail à l'utilisateur
    private function sendEmailToUser($email, $git): void
    {
        // Récupère l'utilisateur actuellement connecté
        $user = $this->getUser();

        // Obtient l'adresse e-mail de l'utilisateur connecté
        $fromEmail = $user->getEmail();

        // Récupère le lien et le sujet du formulaire Git
        $lien = $git->getLien();
        $sujet = $git->getMessage();

        // Crée un e-mail avec les détails
        $email = (new Email())
            ->from(new Address($fromEmail, 'Votre nom'))
            ->to('contact@debeugTonCode.com')
            ->subject('Votre message : ')
            ->text($sujet . "\n" . 'Lien : ' . $lien);

        // Envoie l'e-mail
        $this->mailer->send($email);
    }

    /**
     * @Route("/success-git", name="success_git")
     */
    public function successGit(): Response
    {
        // Affiche la vue de succès
        return $this->render('git/success_git.html.twig');
    }
}
