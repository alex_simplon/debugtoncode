<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImmatController extends AbstractController
{
    #[Route('/immat', name: 'app_immat')]
    public function index(): Response
    {
        return $this->render('immat/index.html.twig', [
            'controller_name' => 'ImmatController',
        ]);
    }
}
