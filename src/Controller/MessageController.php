<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Form\MessagesType;
use App\Repository\MessagesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Query\Expr\OrderBy;

class MessageController extends AbstractController
{
    private $entityManager;
    private $messagesRepository;

    public function __construct(EntityManagerInterface $entityManager, MessagesRepository $messagesRepository)
    {
        $this->entityManager = $entityManager;
        $this->messagesRepository = $messagesRepository;
    }

    #[Route('/message', name: 'app_message')]
    public function index(): Response
    {
        return $this->redirectToRoute('messagerie');
    }

    /**
     * @Route("/send", name="send")
     */
    public function send(): Response
    {
        // Vérifier si l'utilisateur est connecté
        if ($this->getUser()) {
            $messages = $this->getUser()->getSent();

            return $this->render('message/send.html.twig', [
                'messages' => $messages,
            ]);
        } else {
            // Utilisateur non authentifié, afficher le message d'erreur
            return $this->render('message/error.html.twig', [
                'errorMessage' => "Vous devez vous authentifier pour accéder à cette page.",
            ]);
        }
    }
    /**
     * @Route("/messagerie", name="messagerie")
     */
    public function messagerie(Request $request): Response
    {
        $destinataireMessages = $this->entityManager
            ->getRepository(Messages::class)
            ->findBy(
                ['destinataire' => $this->getUser()],
                ['created_at' => 'DESC']
            );

        $message = new Messages();
        $form = $this->createForm(MessagesType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setSender($this->getUser());
            $this->entityManager->persist($message);
            $this->entityManager->flush();
            $this->addFlash("message", "Message envoyé avec succès.");

            return $this->redirectToRoute('messagerie');
        }

        return $this->render('message/messagerie.html.twig', [
            'messages' => $destinataireMessages,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/message/{id}", name="read")
     */
    public function read($id): Response
    {
        $message = $this->entityManager->getRepository(Messages::class)->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Le message n\'existe pas.');
        }

        $message->setIsRead(true);
        $this->entityManager->flush();

        return $this->render('message/read.html.twig', [
            'message' => $message,
        ]);
    }

    /**
     * @Route("/repondre/{id}", name="repondre")
     */
    public function repondre(Request $request, $id): Response
    {
        $message = $this->entityManager->getRepository(Messages::class)->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Le message n\'existe pas.');
        }

        $repondreForm = $this->createForm(MessagesType::class);

        $repondreForm->handleRequest($request);
        if ($repondreForm->isSubmitted() && $repondreForm->isValid()) {
            // Traiter les données du formulaire et effectuer les actions appropriées

            return $this->redirectToRoute('messagerie');
        }

        return $this->render('message/reponsemessage.html.twig', [
            'message' => $message,
            'repondreForm' => $repondreForm->createView(),
        ]);
    }

    /**
     * @Route("/formulaire", name="formulaire")
     */
    public function formulaire(Request $request, ?string $destinatairePseudo): Response
    {
        $user = $this->getUser(); // Récupérer l'utilisateur actuellement connecté
        $message = new Messages();
        $message->setSender($user); // Affecter l'expéditeur

        if ($destinatairePseudo) {
            // Utiliser $recipientPseudo pour rechercher le destinataire et affecter le destinataire à $message->setRecipient()
            // ...
        }

        $form = $this->createForm(MessagesType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($message);
            $this->entityManager->flush();

            // Traiter les données du formulaire et effectuer les actions appropriées

            return $this->redirectToRoute('messagerie');
        }

        return $this->render('message/formulaire.html.twig', [
            'form' => $form->createView(),
            'message' => $message, // Ajoutez la variable 'message' ici
        ]);
    }

    /**
     * @Route("/received", name="received")
     */
    public function received(): Response
    {
        // Vérifier si l'utilisateur est authentifié
        if ($this->getUser()) {
            $messages = $this->getUser()->getDestinataire();
            return $this->render('message/received.html.twig', [
                'messages' => $messages,
            ]);
        } else {
            // Utilisateur non authentifié, afficher le message d'erreur
            return $this->render('message/error.html.twig', [
                'errorMessage' => "Vous devez vous authentifier pour accéder à cette page.",
            ]);
        }
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Messages $message): Response
    {
        $this->entityManager->remove($message);
        $this->entityManager->flush();

        return $this->redirectToRoute("received");
    }

    /**
     * @Route("/navbar", name="navbar")
     */
    public function navbar($id): Response
    {
        $message = $this->entityManager->getRepository(Messages::class)->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Le message n\'existe pas.');
        }

        $message->setIsRead(true);
        $this->entityManager->flush();

        return $this->render('message/read.html.twig', [
            'message' => $message,
        ]);
    }
}
