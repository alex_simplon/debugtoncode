<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlandusiteController extends AbstractController
{
    #[Route('/plandusite', name: 'app_plandusite')]
    public function index(): Response
    {
        return $this->render('plandusite/index.html.twig', [
            'controller_name' => 'PlandusiteController',
        ]);
    }
}
