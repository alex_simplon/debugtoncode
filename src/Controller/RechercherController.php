<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RechercherController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    // Cette annotation indique que cette méthode répond aux requêtes GET sur l'URL '/rechercher'
    #[Route('/rechercher', name: 'rechercher_utilisateurs', methods: ['GET'])]
    public function rechercherUtilisateurs(Request $request): Response
    {
        // Récupère le paramètre 'pseudo' de la requête GET
        $pseudo = $request->query->get('pseudo');

        // Récupère le repository de l'entité User via l'EntityManager
        $userRepository = $this->entityManager->getRepository(User::class);

        // Effectue une requête pour trouver les utilisateurs dont le pseudo commence par la valeur du paramètre 'pseudo'
        $users = $userRepository->createQueryBuilder('u')
            ->where('u.pseudo LIKE :pseudo')
            ->setParameter('pseudo', $pseudo . '%')
            ->getQuery()
            ->getResult();

        // Fait quelque chose avec les utilisateurs trouvés (par exemple, les passer à une vue pour les afficher)

        // Renvoie une vue Twig avec les utilisateurs trouvés comme données à afficher
        return $this->render('rechercher/utilisateurs.html.twig', [
            'users' => $users,
        ]);
    }

    // Cette annotation indique que cette méthode répond aux requêtes GET sur l'URL '/get_user_details'
    #[Route('/get_user_details', name: 'get_user_details', methods: ['GET'])]
    public function getUserDetails(Request $request): JsonResponse
    {
        // Récupère le paramètre 'userId' de la requête GET
        $userId = $request->query->get('userId');

        // Récupère le repository de l'entité User via l'EntityManager
        $userRepository = $this->entityManager->getRepository(User::class);

        // Récupère les détails de l'utilisateur en utilisant son ID
        $user = $userRepository->find($userId);

        // Retourne les détails de l'utilisateur sous forme de réponse JSON
        return new JsonResponse($user);
    }
}
