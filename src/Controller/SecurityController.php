<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    // Cette annotation indique que cette méthode répond aux requêtes GET sur l'URL '/login'
    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // Si l'utilisateur est déjà connecté, on pourrait rediriger vers une autre page (target_path) ici.
        if ($this->getUser()) {
            return $this->redirectToRoute('app_git');
        }

        // Récupère l'erreur de connexion s'il y en a une
        $error = $authenticationUtils->getLastAuthenticationError();
        // Récupère le dernier nom d'utilisateur saisi par l'utilisateur
        $lastUsername = $authenticationUtils->getLastUsername();

        // Rend le template Twig 'security/login.html.twig' avec les données nécessaires
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    // Cette annotation indique que cette méthode répond aux requêtes GET sur l'URL '/logout'
    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        // Cette méthode peut rester vide, car elle sera interceptée par la configuration de sécurité pour la déconnexion.
        throw new \LogicException('Cette méthode peut rester vide - elle sera interceptée par la clé "logout" de votre pare-feu.');
    }
}
