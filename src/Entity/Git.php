<?php

namespace App\Entity;

use App\Repository\GitRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GitRepository::class)] // Déclare que cette classe est une entité gérée par Doctrine, en utilisant le GitRepository pour l'accès aux données
class Git
{
    #[ORM\Id] // Marque la propriété comme clé primaire
    #[ORM\GeneratedValue] // Indique que la valeur est générée automatiquement
    #[ORM\Column] // Marque la propriété comme une colonne dans la base de données
    private ?int $id = null; // Identifiant de l'objet Git

    #[ORM\Column(length: 255)] // Colonne de type chaîne de caractères avec une longueur maximale de 255 caractères
    private ?string $Lien = null; // Lien associé à l'objet Git

    #[ORM\Column(type: Types::TEXT)] // Colonne de type texte
    private ?string $Message = null; // Message associé à l'objet Git

    #[ORM\Column(length: 255, nullable: true)] // Colonne de type chaîne de caractères avec une longueur maximale de 255 caractères et autorisant la valeur nulle
    private ?string $email = null; // Adresse email associée à l'objet Git

    #[ORM\ManyToOne(inversedBy: 'userId')] // Relation ManyToOne : plusieurs objets Git peuvent être associés à un utilisateur
    #[ORM\JoinColumn(nullable: false)] // Clé étrangère non nullable dans la base de données
    private ?User $user = null; // Utilisateur associé à l'objet Git

    // Méthodes
    public function getId(): ?int
    {
        return $this->id; // Renvoie l'identifiant de l'objet Git
    }

    public function getLien(): ?string
    {
        return $this->Lien; // Renvoie le lien de l'objet Git
    }

    public function setLien(string $Lien): static
    {
        $this->Lien = $Lien; // Définit le lien de l'objet Git

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->Message; // Renvoie le message de l'objet Git
    }

    public function setMessage(string $Message): static
    {
        $this->Message = $Message; // Définit le message de l'objet Git

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email; // Renvoie l'adresse email associée à l'objet Git
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email; // Définit l'adresse email associée à l'objet Git
    }

    public function getUser(): ?User
    {
        return $this->user; // Renvoie l'utilisateur associé à l'objet Git
    }

    public function setUser(?User $user): self
    {
        $this->user = $user; // Définit l'utilisateur associé à l'objet Git

        return $this;
    }
}
