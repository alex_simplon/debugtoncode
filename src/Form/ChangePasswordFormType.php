<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    // Méthode pour construire le formulaire
    // Méthode pour construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Ajout d'un champ 'plainPassword' de type RepeatedType (champ de confirmation du mot de passe)
        $builder->add('plainPassword', RepeatedType::class, [

            // Spécification du type de champ à l'intérieur de RepeatedType (PasswordType dans ce cas)
            'type' => PasswordType::class,

            // Options supplémentaires pour le champ
            'options' => [
                'attr' => [
                    'autocomplete' => 'new-password', // Attribut HTML pour le champ d'autocomplétion du nouveau mot de passe
                ],
            ],

            // Options du premier champ du formulaire (nouveau mot de passe)
            'first_options' => [
                'constraints' => [
                    new NotBlank([ // Contrainte : le champ ne doit pas être vide
                        'message' => 'Veuillez entrer un mot de passe.',
                    ]),
                    new Length([ // Contrainte : longueur minimale et maximale du mot de passe
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères.',
                        'max' => 4096,
                    ]),
                ],
                'label' => 'Nouveau mot de passe', // Étiquette pour le champ du premier mot de passe
            ],

            // Options du second champ du formulaire (confirmation du mot de passe)
            'second_options' => [
                'label' => 'Confirmez le nouveau mot de passe', // Étiquette pour le champ de confirmation du mot de passe
            ],

            'invalid_message' => 'Les mots de passe ne correspondent pas.', // Message en cas de mots de passe non correspondants
            'mapped' => false, // Ne pas lier ce champ à une propriété de l'entité (ce n'est pas stocké dans l'entité)
        ]);
    }


    // Méthode pour configurer les options du formulaire
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]); // Définir les options par défaut du formulaire
    }
}
