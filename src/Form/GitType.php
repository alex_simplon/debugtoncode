<?php

namespace App\Form;

use App\Entity\Git;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class GitType extends AbstractType
{
    // Méthode pour construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Utilisation de $builder pour ajouter les champs au formulaire
        $builder
            ->add('Lien', TextType::class, [
                'attr' => [
                    'placeholder' => 'Déposes ton Lien Git ici',
                ],
                'mapped' => true, // Lier le champ à la propriété 'Lien' de l'entité
                'constraints' => [
                    new Callback([$this, 'validateGitLink']), // Ajout de la contrainte de validation personnalisée
                ],
            ])
            ->add('Message', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Déposes ton Message ici',
                ],
                'mapped' => true, // Lier le champ au champ 'Message' de l'entité
            ]);
    }

    // Méthode pour configurer les options du formulaire
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Git::class, // Lier les données au modèle Git
        ]);
    }

    // Fonction de validation personnalisée pour le champ 'Lien' du formulaire
    public function validateGitLink($value, ExecutionContextInterface $context): void
    {
        $url = rtrim($value, '/'); // Supprimer le slash à la fin de l'URL

        // Vérifier si l'URL commence par 'https://gitlab.com'
        if (false === strpos($url, 'https://gitlab.com')) {
            $context->buildViolation('L\'URL doit commencer par "https://gitlab.com/"')
                ->atPath('Lien') // Champ associé à la violation
                ->addViolation(); // Ajouter la violation au contexte de validation
        }
    }
}
