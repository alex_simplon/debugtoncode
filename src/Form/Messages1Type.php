<?php

namespace App\Form;

use App\Entity\Messages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Messages1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sujet', null, [
                'attr' => ['placeholder' => 'Sujet du message'],
            ])
            ->add('message', null, [
                'attr' => ['placeholder' => 'Contenu du message'],
            ])
            ->add('created_at', null, [
                'label' => 'Date de création',
            ])
            ->add('is_read', null, [
                'label' => 'Message lu',
            ])
            ->add('destinataire', null, [
                'attr' => ['placeholder' => 'Destinataire'],
                'label' => 'Destinataire',
            ])
            ->add('sender', null, [ // Adding the sender field
                'attr' => ['placeholder' => 'Expéditeur'],
                'label' => 'Expéditeur',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Messages::class,
        ]);
    }
}
