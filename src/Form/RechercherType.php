<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercherType extends AbstractType
{
    // Méthode pour construire le formulaire
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Ajout du champ 'pseudo' de type TextType (champ de texte simple)
        $builder->add('pseudo', TextType::class);

        // Ajout du champ 'roles' de type ChoiceType (champ de sélection multiple)
        $builder->add('roles', ChoiceType::class, [
            'choices' => [
                'Role 1' => 'ROLE_1',
                'Role 2' => 'ROLE_2',
                // Ajoutez plus de rôles au besoin
            ],
            'multiple' => true,  // Permettre plusieurs choix
            'expanded' => true,  // Afficher les choix sous forme de boutons
        ]);
    }

    // Méthode pour configurer les options du formulaire
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,  // Lier les données au modèle User
        ]);
    }
}
