<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, [
                'label' => 'Réponse',
                'attr' => [
                    'class' => 'form-control',
                ],
            ]);

        // Custom logic using the passed options
        $expediteur = $options['expediteur'];
        $message = $options['message'];

        // ... add your custom logic here
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'expediteur' => null,
            'message' => null,
        ]);
    }
}
