<?php

namespace App\Repository;

use App\Entity\Git;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository personnalisé pour l'entité Git.
 * @extends ServiceEntityRepository<Git>
 *
 * @method Git|null find($id, $lockMode = null, $lockVersion = null)
 * @method Git|null findOneBy(array $criteria, array $orderBy = null)
 * @method Git[]    findAll()
 * @method Git[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GitRepository extends ServiceEntityRepository
{
    // Constructeur du repository
    public function __construct(ManagerRegistry $registry)
    {
        // Appelle le constructeur parent avec l'entité cible
        parent::__construct($registry, Git::class);
    }

    // Méthode pour sauvegarder l'entité Git
    public function save(Git $entity, bool $flush = false): void
    {
        // Ajoute l'entité à la gestion de l'EntityManager
        $this->getEntityManager()->persist($entity);

        // Si $flush est vrai, exécute le processus de flush pour persister les changements dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // Méthode pour supprimer l'entité Git
    public function remove(Git $entity, bool $flush = false): void
    {
        // Marque l'entité comme étant supprimée
        $this->getEntityManager()->remove($entity);

        // Si $flush est vrai, exécute le processus de flush pour effectuer la suppression dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
