<?php

namespace App\Repository;

use App\Entity\Messages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Repository personnalisé pour l'entité Messages.
 * @extends ServiceEntityRepository<Messages>
 *
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    // Constructeur du repository
    public function __construct(ManagerRegistry $registry)
    {
        // Appelle le constructeur parent avec l'entité cible
        parent::__construct($registry, Messages::class);
    }

    // Méthode pour sauvegarder l'entité Messages
    public function save(Messages $entity, bool $flush = false): void
    {
        // Ajoute l'entité à la gestion de l'EntityManager
        $this->getEntityManager()->persist($entity);

        // Si $flush est vrai, exécute le processus de flush pour persister les changements dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // Méthode pour supprimer l'entité Messages
    public function remove(Messages $entity, bool $flush = false): void
    {
        // Marque l'entité comme étant supprimée
        $this->getEntityManager()->remove($entity);

        // Si $flush est vrai, exécute le processus de flush pour effectuer la suppression dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // Méthode pour récupérer les messages non lus d'un utilisateur donné
    public function findUnreadMessagesForUser(User $user): array
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.destinataire = :user')
            ->andWhere('m.is_read = false')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
