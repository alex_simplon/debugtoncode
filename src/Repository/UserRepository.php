<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * Repository personnalisé pour l'entité User.
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    // Constructeur du repository
    public function __construct(ManagerRegistry $registry)
    {
        // Appelle le constructeur parent avec l'entité cible
        parent::__construct($registry, User::class);
    }

    // Méthode pour sauvegarder l'entité User
    public function save(User $entity, bool $flush = false): void
    {
        // Ajoute l'entité à la gestion de l'EntityManager
        $this->getEntityManager()->persist($entity);

        // Si $flush est vrai, exécute le processus de flush pour persister les changements dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    // Méthode pour supprimer l'entité User
    public function remove(User $entity, bool $flush = false): void
    {
        // Marque l'entité comme étant supprimée
        $this->getEntityManager()->remove($entity);

        // Si $flush est vrai, exécute le processus de flush pour effectuer la suppression dans la base de données
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Utilisée pour mettre à niveau (ré-hacher) automatiquement le mot de passe de l'utilisateur au fil du temps.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        // Vérifie si l'utilisateur est une instance de User
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        // Met à jour le mot de passe haché de l'utilisateur avec le nouveau mot de passe haché
        $user->setPassword($newHashedPassword);

        // Sauvegarde l'utilisateur avec le nouveau mot de passe haché
        $this->save($user, true);
    }
}
